#!/bin/bash
# Ynov
# backup script

declare -r target_path="${1}"
declare -r target_dirname=$(awk -F'/' '{ print $NF }' <<< "${target_path%/}")

declare -r backup_destination_dir="/opt/backup/"
declare -r backup_date="$(date +%y%m%d_%H%M%S)"
declare -r backup_filename="${target_dirname}_${backup_date}.tar.gz"
declare -r backup_destination_path="${backup_destination_dir}/${backup_filename}"

declare -r backup_user_name="backup"
declare -ri backup_user_uid=1003
declare -ri backup_user_umask=077

declare -i backups_quantity=7
declare -ri backups_quantity=$((backups_quantity+1))

archive_and_compress() {
  dir_to_backup="${1}"

  tar cvzf \
    "${backup_destination_path}" \
    "${dir_to_backup}" \
    --ignore-failed-read &> /dev/null

  if [[ $? -eq 0 ]]
  then
    log "Backup ${backup_filename} has been saved to ${backup_destination_dir}."
  else 
    log "Backup ${backup_filename} has failed."
    rm -f "${backup_destination_path}"
    exit 1
  fi
}

# Backup cleaner
clean_older() {
  oldest=$(ls -tp "${backup_destination_dir}" | grep -v '/$' | grep -E "^${target_dirname}.*$" | tail -n +${backups_quantity})

  if [[ ! -z $oldest ]]
  then
    for backup_to_del in ${oldest}
    do
      rm -f "${backup_destination_dir}/${backup_to_del}" &> /dev/null
      if [[ $? -eq 0 ]]
      then
        log "Backup ${backup_to_del} has been removed from ${backup_destination_dir}."
      else
        log "Deletion of backup has failed."
        exit 1
      fi
    done
  fi
}

if [[ ${EUID} -ne ${backup_user_uid} ]]; then
  exit 1
fi
if [[ ! -d "${target_path}" ]]; then
  exit 1
fi
if [[ ! -r "${target_path}" ]]; then
  exit 1
fi

if [[ ! -d "${backup_destination_dir}" ]]; then
  exit 1
fi
if [[ ! -w "${backup_destination_dir}" ]]; then
  exit 1
fi

archive_and_compress "${target_path}"
clean_older
