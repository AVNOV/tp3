#!/bin/bash

echo "127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
127.0.1.1 node1.tp2.b2
192.168.1.12 node2.tp2.b2" > "/etc/hosts"

# User
useradd admin -m
usermod -aG wheel admin

#Firewall
systemctl start firewalld

# install
yum update -y
yum install vim-enhanced -y
yum install -y epel-release
yum install -y nginx
