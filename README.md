﻿

# I. Services systemd


## 1.1 Basics

🌞 Utilisez la ligne de commande pour sortir les infos suivantes :


|CLI| afficher le nombre de services systemd dispos sur la machine :

    [vagrant@node1 ~]$ systemctl list-units --all --type=service | grep "units listed"| cut -d' ' -f1

|CLI| afficher le nombre de services systemd actifs ("running") sur la machine : 

	[vagrant@node1 ~]$ systemctl list-units --all --type=service --state=running | grep "units listed" | cut -d' ' -f1

|CLI| afficher le nombre de services systemd qui ont échoué ("failed") ou inactifs ("exited") sur la machine 		

    [vagrant@node1 ~]$ systemctl list-units --all --type=service --state=failed --state=exited | grep "units listed" | cut -d' ' -f1

|CLI| afficher la liste des services systemd qui démarrent automatiquement au boot ("enabled") :

	[vagrant@node1 ~]$ systemctl list-unit-files --all --type=service | grep "enabled"


## 2 Analyse de service systemd

🌞 Etudiez le service nginx.service

 1. déterminer le path de l'unité nginx.service 
   
	    [vagrant@node1 ~]$ systemctl status nginx
	    ● nginx.service - The nginx HTTP and reverse proxy server
	       Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
	       Active: inactive (dead)

		
 2. afficher son contenu et expliquer les lignes qui comportent :

-ExecStart :
				Le gestionnaire de services demarre l'unité immédiatement après que le processus de service principal ai été interrompu.				
-ExecStartPre :
				Commande executé avant ExecStart.
-PIDFile :
				Où trouver l'identifiant PID du processus du service
-Type :
				Configure le type de démarrage du processus pour cette unité de service.
-ExecReload :
				Processus à demarrer au reload du service.
-Description :
				Description du service.
-After :
				Permet d'indiquer quel pré-requis est nécessaire pour le fonctionnement du service

    [vagrant@node1 ~]$ cat /usr/lib/systemd/system/nginx.service
    [Unit]
    Description=The nginx HTTP and reverse proxy server
    After=network.target remote-fs.target nss-lookup.target
    
    [Service]
    Type=forking
    PIDFile=/run/nginx.pid
    
    Nginx will fail to start if /run/nginx.pid already exists but has the wrong 
    SELinux context. This might happen when running `nginx -t` from the cmdline. https://bugzilla.redhat.com/show_bug.cgi?id=1268621
    
    ExecStartPre=/usr/bin/rm -f /run/nginx.pid
    ExecStartPre=/usr/sbin/nginx -t
    ExecStart=/usr/sbin/nginx
    ExecReload=/bin/kill -s HUP $MAINPID
    KillSignal=SIGQUIT
    TimeoutStopSec=5
    KillMode=process
    PrivateTmp=true
    
    [Install]
    WantedBy=multi-user.target
___

🌞 **|CLI|** Listez tous les services qui contiennent la ligne `WantedBy=multi-user.target`:

Se rendre dans /usr/lib/systemd/system et executer :

		sudo find / -type f -name '*service' -exec grep -H 'WantedBy=multi-user.target' {} \; 2> /dev/null | cut -d ':' -f1

___

## 3. Création d'un service

Pour créer un service, il suffit de créer un fichier dans `/etc/systemd/system/` .
L'endroit qui est dédié à la création de services par l'administrateur.

    [Unit]
    Description=Server web handler
    Require=firewalld.service
    
    User=admin
    Environment=PORT=8080
    
    [Service]
    Type=simple
    
    ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=8080/tcp
    ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --reload
    ExecStart=/usr/bin/python2 -m SimpleHTTPServer 8888
    ExecStopPost=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=8080/tcp
    ExecStopPost=/usr/bin/sudo /usr/bin/firewall-cmd --reload
	
	[Install]
	WantedBy=multi-user.target

Allumage du service :
   
    [admin@node1 vagrant]$ sudo systemctl start setup-server

Puis check de l'état de celui-ci : 


    [admin@node1 vagrant]$ sudo systemctl status setup-server.service
    ● setup-server.service - Server web handler
       Loaded: loaded (/etc/systemd/system/setup-server.service; static; vendor preset: disabled)
       Active: active (running) since Sun 2020-10-11 02:39:02 UTC; 7min ago
     Main PID: 2933 (python2)
       CGroup: /system.slice/setup-server.service
               └─2933 /usr/bin/python2 -m SimpleHTTPServer 8888
    
    Oct 11 02:39:01 node1.tp3.b2 systemd[1]: [/etc/systemd/system/setup-server.service:3] Unknown lvalue 'Require'...Unit'
    Oct 11 02:39:01 node1.tp3.b2 systemd[1]: [/etc/systemd/system/setup-server.service:5] Unknown lvalue 'User' in...Unit'
    Oct 11 02:39:01 node1.tp3.b2 systemd[1]: Starting Server web handler...
    Oct 11 02:39:01 node1.tp3.b2 sudo[2894]:     root : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/firewal...0/tcp
    Oct 11 02:39:01 node1.tp3.b2 sudo[2901]:     root : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/firewal...eload
    Oct 11 02:39:02 node1.tp3.b2 systemd[1]: Started Server web handler.
    Oct 11 02:39:44 node1.tp3.b2 systemd[1]: [/etc/systemd/system/setup-server.service:3] Unknown lvalue 'Require'...Unit'
    Oct 11 02:39:44 node1.tp3.b2 systemd[1]: [/etc/systemd/system/setup-server.service:5] Unknown lvalue 'User' in...Unit'
    Oct 11 02:40:40 node1.tp3.b2 systemd[1]: [/etc/systemd/system/setup-server.service:3] Unknown lvalue 'Require'...Unit'
    Oct 11 02:40:40 node1.tp3.b2 systemd[1]: [/etc/systemd/system/setup-server.service:5] Unknown lvalue 'User' in...Unit'
    Hint: Some lines were ellipsized, use -l to show in full.


Curl sur l'ip de la machine avec le port du SimpleHTTPServer 8888 :

    [admin@node1 vagrant]$ curl 192.168.2.11:8888
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
    <title>Directory listing for /</title>
    <body>
    <h2>Directory listing for /</h2>
    <hr>
    <ul>
    <li><a href="bin/">bin@</a>
    <li><a href="boot/">boot/</a>
    <li><a href="dev/">dev/</a>
    <li><a href="etc/">etc/</a>
    <li><a href="home/">home/</a>
    <li><a href="lib/">lib@</a>
    <li><a href="lib64/">lib64@</a>
    <li><a href="media/">media/</a>
    <li><a href="mnt/">mnt/</a>
    <li><a href="opt/">opt/</a>
    <li><a href="proc/">proc/</a>
    <li><a href="root/">root/</a>
    <li><a href="run/">run/</a>
    <li><a href="sbin/">sbin@</a>
    <li><a href="srv/">srv/</a>
    <li><a href="swapfile">swapfile</a>
    <li><a href="sys/">sys/</a>
    <li><a href="tmp/">tmp/</a>
    <li><a href="usr/">usr/</a>
    <li><a href="vagrant/">vagrant/</a>
    <li><a href="var/">var/</a>
    </ul>
    <hr>
    </body>
    </html>

___

Script de backup du tp1 :

	#!/bin/bash
    # Ynov
    # backup script
    
    declare -r target_path="${1}"
    declare -r target_dirname=$(awk -F'/' '{ print $NF }' <<< "${target_path%/}")
    
    declare -r backup_destination_dir="/opt/backup/"
    declare -r backup_date="$(date +%y%m%d_%H%M%S)"
    declare -r backup_filename="${target_dirname}_${backup_date}.tar.gz"
    declare -r backup_destination_path="${backup_destination_dir}/${backup_filename}"
    
    declare -r backup_user_name="backup"
    declare -ri backup_user_uid=1003
    declare -ri backup_user_umask=077
    
    declare -i backups_quantity=7
    declare -ri backups_quantity=$((backups_quantity+1))
    
    archive_and_compress() {
      dir_to_backup="${1}"
    
    tar cvzf \
      "${backup_destination_path}" \
      "${dir_to_backup}" \
      --ignore-failed-read &> /dev/null
    
    if [[ $? -eq 0 ]]
    then
      log "Backup ${backup_filename} has been saved to ${backup_destination_dir}."
    else 
      log "Backup ${backup_filename} has failed."
      rm -f "${backup_destination_path}"
      exit 1
    fi
    }
    
    clean_older() {
      oldest=$(ls -tp "${backup_destination_dir}" | grep -v '/$' | grep -E "^${target_dirname}.*$" | tail -n +${backups_quantity})
      
      if [[ ! -z $oldest ]]
      then
       for backup_to_del in ${oldest}
       do
         rm -f "${backup_destination_dir}/${backup_to_del}" &> /dev/null
         if [[ $? -eq 0 ]]
         then
	       log "Backup ${backup_to_del} has been removed from ${backup_destination_dir}."
         else
           log "Deletion of backup has failed."
	       exit 1
         fi
	   done
	  fi
	}
        
    if [[ ${EUID} -ne ${backup_user_uid} ]]; then
      exit 1
    fi
    if [[ ! -d "${target_path}" ]]; then
      exit 1
    fi
    if [[ ! -r "${target_path}" ]]; then
      exit 1
    fi
    
    if [[ ! -d "${backup_destination_dir}" ]]; then
      exit 1
    fi
    if [[ ! -w "${backup_destination_dir}" ]]; then
      exit 1
    fi
    
    archive_and_compress "${target_path}"
    clean_older

## Sauvegarde
Un script qui se lance AVANT la sauvegarde, qui effectue les tests :

    #!/bin/bash
    # Script de test d'avant sauvegarde #
    
    if [ ! -d "/srv/site1/" ]; then
            echo "The repertory /srv/site1 does not exist..."
            exit 7
    else
            mkdir -p /home/admin/save_tp3
    fi

Un script de sauvegarde :
    
    #!/bin/bash                                                                                                           
    # Mise en archive et compression de notre fichier puis sauvegarde #                                                   
    
    echo $$ > /var/run/own_service_pid/pid
    tar -czf /home/admin/save_tp3/site1_$(date +"%Y%m%d_%H%M%S").tar.gz /srv/site1

Un script qui s'exécute APRES la sauvegardequi tri et qui ne garde que les 7 sauvegardes les plus récentes :

    #!/bin/bash
    # Script de nettoyage des sauvegardes #
    
    if [[ $(ls -1 /home/admin/save_tp3 | wc -l) == 8 ]]
    then
            rm "$(ls -t | tail -1)"
    fi
Le service tp3_backup.sh :

    [Unit]
    Description=Service qui va lancer un script de sauvegarde
    
    [Service]
    Environment="BEFORE_TEST=/home/admin_tp3/before_save.sh"
    Environment="START_SAVE=/home/admin_tp3/tp3_backup.sh"
    Environment="CLEAN_SAVES=/home/admin_tp3/after_save.sh"
    
    User=admin
    
    PIDFile=/var/run/own_pid/pid
    ExecStartPre=/usr/bin/sh ${BEFORE_TEST}
    ExecStart=/usr/bin/sh ${START_SAVE}
    ExecStopPost=/usr/bin/sh ${CLEAN_SAVES}
    
    [Install]
    WantedBy=multi-user.target
___

## FEATURES

 Utilisation de `systemd-analyze plot` pour récupérer une diagramme du boot, au format SVG :


    [admin@features ~]$ systemd-analyze plot
    <?xml version="1.0" standalone="no"?>
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
    <svg width="884px" height="1670px" version="1.1" xmlns="http://www.w3.org/2000/svg">
    
    <!-- This file is a systemd-analyze SVG file. It is best rendered in a   -->
    <!-- browser such as Chrome, Chromium or Firefox. Other applications     -->
    <!-- that render these files properly but much slower are ImageMagick,   -->
    <!-- gimp, inkscape, etc. To display the files on your system, just      -->
    <!-- point your browser to this file.                                    -->
    
    <!-- This plot was generated by systemd-analyze version 239              -->
    
    <defs>
      <style type="text/css">
        <![CDATA[
          rect       { stroke-width: 1; stroke-opacity: 0; }
          rect.background   { fill: rgb(255,255,255); }
          rect.activating   { fill: rgb(255,0,0); fill-opacity: 0.7; }
          rect.active       { fill: rgb(200,150,150); fill-opacity: 0.7; }
          rect.deactivating { fill: rgb(150,100,100); fill-opacity: 0.7; }
          rect.kernel       { fill: rgb(150,150,150); fill-opacity: 0.7; }
          rect.initrd       { fill: rgb(150,150,150); fill-opacity: 0.7; }
          rect.firmware     { fill: rgb(150,150,150); fill-opacity: 0.7; }
          rect.loader       { fill: rgb(150,150,150); fill-opacity: 0.7; }
          rect.userspace    { fill: rgb(150,150,150); fill-opacity: 0.7; }
          rect.security     { fill: rgb(144,238,144); fill-opacity: 0.7; }
          rect.generators   { fill: rgb(102,204,255); fill-opacity: 0.7; }
          rect.unitsload    { fill: rgb( 82,184,255); fill-opacity: 0.7; }
          rect.box   { fill: rgb(240,240,240); stroke: rgb(192,192,192); }
          line       { stroke: rgb(64,64,64); stroke-width: 1; }
    //    line.sec1  { }
          line.sec5  { stroke-width: 2; }
          line.sec01 { stroke: rgb(224,224,224); stroke-width: 1; }
          text       { font-family: Verdana, Helvetica; font-size: 14px; }
          text.left  { font-family: Verdana, Helvetica; font-size: 14px; text-anchor: start; }
          text.right { font-family: Verdana, Helvetica; font-size: 14px; text-anchor: end; }
          text.sec   { font-size: 10px; }
        ]]>
       </style>
    </defs>
    
    <rect class="background" width="100%" height="100%" />
    <text x="20" y="50">Startup finished in 698ms (kernel) + 2.137s (initrd) + 5.208s (userspace) = 8.043s
    multi-user.target reached after 5.159s in userspace</text><text x="20" y="30">CentOS Linux 8 (Core) features.tp3.b2 (Linux 4.18.0-80.el8.x86_64 #1 SMP Tue Jun 4 09:19:46 UTC 2019) x86-64 oracle</text><g transform="translate(20.000,100)">
    <rect class="box" x="0" y="0" width="804.370" height="1420.000" />
      <line class="sec5" x1="0.000" y1="0" x2="0.000" y2="1420.000" />
      <text class="sec" x="0.000" y="-5.000" >0.0s</text>
      <line class="sec01" x1="10.000" y1="0" x2="10.000" y2="1420.000" />
      <line class="sec01" x1="20.000" y1="0" x2="20.000" y2="1420.000" />
      <line class="sec01" x1="30.000" y1="0" x2="30.000" y2="1420.000" />
      <line class="sec01" x1="40.000" y1="0" x2="40.000" y2="1420.000" />
      <line class="sec01" x1="50.000" y1="0" x2="50.000" y2="1420.000" />
      <line class="sec01" x1="60.000" y1="0" x2="60.000" y2="1420.000" />
      <line class="sec01" x1="70.000" y1="0" x2="70.000" y2="1420.000" />
      <line class="sec01" x1="80.000" y1="0" x2="80.000" y2="1420.000" />
      <line class="sec01" x1="90.000" y1="0" x2="90.000" y2="1420.000" />
      <line class="sec1" x1="100.000" y1="0" x2="100.000" y2="1420.000" />
      <text class="sec" x="100.000" y="-5.000" >1.0s</text>
      <line class="sec01" x1="110.000" y1="0" x2="110.000" y2="1420.000" />
      <line class="sec01" x1="120.000" y1="0" x2="120.000" y2="1420.000" />
      <line class="sec01" x1="130.000" y1="0" x2="130.000" y2="1420.000" />
      <line class="sec01" x1="140.000" y1="0" x2="140.000" y2="1420.000" />
      <line class="sec01" x1="150.000" y1="0" x2="150.000" y2="1420.000" />
      <line class="sec01" x1="160.000" y1="0" x2="160.000" y2="1420.000" />
      <line class="sec01" x1="170.000" y1="0" x2="170.000" y2="1420.000" />
      <line class="sec01" x1="180.000" y1="0" x2="180.000" y2="1420.000" />
      <line class="sec01" x1="190.000" y1="0" x2="190.000" y2="1420.000" />
      <line class="sec1" x1="200.000" y1="0" x2="200.000" y2="1420.000" />
      <text class="sec" x="200.000" y="-5.000" >2.0s</text>
      <line class="sec01" x1="210.000" y1="0" x2="210.000" y2="1420.000" />
      <line class="sec01" x1="220.000" y1="0" x2="220.000" y2="1420.000" />
      <line class="sec01" x1="230.000" y1="0" x2="230.000" y2="1420.000" />
      <line class="sec01" x1="240.000" y1="0" x2="240.000" y2="1420.000" />
      <line class="sec01" x1="250.000" y1="0" x2="250.000" y2="1420.000" />
      <line class="sec01" x1="260.000" y1="0" x2="260.000" y2="1420.000" />
      <line class="sec01" x1="270.000" y1="0" x2="270.000" y2="1420.000" />
      <line class="sec01" x1="280.000" y1="0" x2="280.000" y2="1420.000" />
      <line class="sec01" x1="290.000" y1="0" x2="290.000" y2="1420.000" />
      <line class="sec1" x1="300.000" y1="0" x2="300.000" y2="1420.000" />
      <text class="sec" x="300.000" y="-5.000" >3.0s</text>
      <line class="sec01" x1="310.000" y1="0" x2="310.000" y2="1420.000" />
      <line class="sec01" x1="320.000" y1="0" x2="320.000" y2="1420.000" />
      <line class="sec01" x1="330.000" y1="0" x2="330.000" y2="1420.000" />
      <line class="sec01" x1="340.000" y1="0" x2="340.000" y2="1420.000" />
      <line class="sec01" x1="350.000" y1="0" x2="350.000" y2="1420.000" />
      <line class="sec01" x1="360.000" y1="0" x2="360.000" y2="1420.000" />
      <line class="sec01" x1="370.000" y1="0" x2="370.000" y2="1420.000" />
      <line class="sec01" x1="380.000" y1="0" x2="380.000" y2="1420.000" />
      <line class="sec01" x1="390.000" y1="0" x2="390.000" y2="1420.000" />
      <line class="sec1" x1="400.000" y1="0" x2="400.000" y2="1420.000" />
      <text class="sec" x="400.000" y="-5.000" >4.0s</text>
      <line class="sec01" x1="410.000" y1="0" x2="410.000" y2="1420.000" />
      <line class="sec01" x1="420.000" y1="0" x2="420.000" y2="1420.000" />
      <line class="sec01" x1="430.000" y1="0" x2="430.000" y2="1420.000" />
      <line class="sec01" x1="440.000" y1="0" x2="440.000" y2="1420.000" />
      <line class="sec01" x1="450.000" y1="0" x2="450.000" y2="1420.000" />
      <line class="sec01" x1="460.000" y1="0" x2="460.000" y2="1420.000" />
      <line class="sec01" x1="470.000" y1="0" x2="470.000" y2="1420.000" />
      <line class="sec01" x1="480.000" y1="0" x2="480.000" y2="1420.000" />
      <line class="sec01" x1="490.000" y1="0" x2="490.000" y2="1420.000" />
      <line class="sec5" x1="500.000" y1="0" x2="500.000" y2="1420.000" />
      <text class="sec" x="500.000" y="-5.000" >5.0s</text>
      <line class="sec01" x1="510.000" y1="0" x2="510.000" y2="1420.000" />
      <line class="sec01" x1="520.000" y1="0" x2="520.000" y2="1420.000" />
      <line class="sec01" x1="530.000" y1="0" x2="530.000" y2="1420.000" />
      <line class="sec01" x1="540.000" y1="0" x2="540.000" y2="1420.000" />
      <line class="sec01" x1="550.000" y1="0" x2="550.000" y2="1420.000" />
      <line class="sec01" x1="560.000" y1="0" x2="560.000" y2="1420.000" />
      <line class="sec01" x1="570.000" y1="0" x2="570.000" y2="1420.000" />
      <line class="sec01" x1="580.000" y1="0" x2="580.000" y2="1420.000" />
      <line class="sec01" x1="590.000" y1="0" x2="590.000" y2="1420.000" />
      <line class="sec1" x1="600.000" y1="0" x2="600.000" y2="1420.000" />
      <text class="sec" x="600.000" y="-5.000" >6.0s</text>
      <line class="sec01" x1="610.000" y1="0" x2="610.000" y2="1420.000" />
      <line class="sec01" x1="620.000" y1="0" x2="620.000" y2="1420.000" />
      <line class="sec01" x1="630.000" y1="0" x2="630.000" y2="1420.000" />
      <line class="sec01" x1="640.000" y1="0" x2="640.000" y2="1420.000" />
      <line class="sec01" x1="650.000" y1="0" x2="650.000" y2="1420.000" />
      <line class="sec01" x1="660.000" y1="0" x2="660.000" y2="1420.000" />
      <line class="sec01" x1="670.000" y1="0" x2="670.000" y2="1420.000" />
      <line class="sec01" x1="680.000" y1="0" x2="680.000" y2="1420.000" />
      <line class="sec01" x1="690.000" y1="0" x2="690.000" y2="1420.000" />
      <line class="sec1" x1="700.000" y1="0" x2="700.000" y2="1420.000" />
      <text class="sec" x="700.000" y="-5.000" >7.0s</text>
      <line class="sec01" x1="710.000" y1="0" x2="710.000" y2="1420.000" />
      <line class="sec01" x1="720.000" y1="0" x2="720.000" y2="1420.000" />
      <line class="sec01" x1="730.000" y1="0" x2="730.000" y2="1420.000" />
      <line class="sec01" x1="740.000" y1="0" x2="740.000" y2="1420.000" />
      <line class="sec01" x1="750.000" y1="0" x2="750.000" y2="1420.000" />
      <line class="sec01" x1="760.000" y1="0" x2="760.000" y2="1420.000" />
      <line class="sec01" x1="770.000" y1="0" x2="770.000" y2="1420.000" />
      <line class="sec01" x1="780.000" y1="0" x2="780.000" y2="1420.000" />
      <line class="sec01" x1="790.000" y1="0" x2="790.000" y2="1420.000" />
      <line class="sec1" x1="800.000" y1="0" x2="800.000" y2="1420.000" />
      <text class="sec" x="800.000" y="-5.000" >8.0s</text>
      <rect class="kernel" x="0.000" y="0.000" width="69.859" height="19.000" />
      <text class="left" x="5.000" y="14.000">kernel</text>
      <rect class="initrd" x="69.859" y="20.000" width="213.711" height="19.000" />
      <text class="left" x="74.859" y="34.000">initrd</text>
      <rect class="active" x="283.570" y="40.000" width="520.800" height="19.000" />
      <rect class="security" x="69.889" y="40.000" width="0.002" height="19.000" />
      <rect class="generators" x="70.753" y="40.000" width="4.482" height="19.000" />
      <rect class="unitsload" x="75.251" y="40.000" width="0.043" height="19.000" />
      <text class="left" x="288.570" y="54.000">systemd</text>
      <rect class="activating" x="357.247" y="60.000" width="11.758" height="19.000" />
      <rect class="active" x="369.005" y="60.000" width="435.366" height="19.000" />
      <rect class="deactivating" x="804.370" y="60.000" width="0.000" height="19.000" />
      <text class="left" x="362.247" y="74.000">systemd-journald.service (117ms)</text>
      <rect class="activating" x="357.286" y="80.000" width="0.000" height="19.000" />
      <rect class="active" x="357.286" y="80.000" width="447.084" height="19.000" />
      <rect class="deactivating" x="804.370" y="80.000" width="0.000" height="19.000" />
      <text class="left" x="362.286" y="94.000">systemd-ask-password-wall.path</text>
      <rect class="activating" x="357.433" y="100.000" width="7.714" height="19.000" />
      <rect class="active" x="365.147" y="100.000" width="439.224" height="19.000" />
      <rect class="deactivating" x="804.370" y="100.000" width="0.000" height="19.000" />
      <text class="left" x="362.433" y="114.000">kmod-static-nodes.service (77ms)</text>
      <rect class="activating" x="357.632" y="120.000" width="7.551" height="19.000" />
      <rect class="active" x="365.184" y="120.000" width="439.187" height="19.000" />
      <rect class="deactivating" x="804.370" y="120.000" width="0.000" height="19.000" />
      <text class="left" x="362.632" y="134.000">dev-hugepages.mount (75ms)</text>
      <rect class="activating" x="358.434" y="140.000" width="6.761" height="19.000" />
      <rect class="active" x="365.195" y="140.000" width="439.175" height="19.000" />
      <rect class="deactivating" x="804.370" y="140.000" width="0.000" height="19.000" />
      <text class="left" x="363.434" y="154.000">sys-kernel-debug.mount (67ms)</text>
      <rect class="activating" x="358.780" y="160.000" width="0.000" height="19.000" />
      <rect class="active" x="358.780" y="160.000" width="445.590" height="19.000" />
      <rect class="deactivating" x="804.370" y="160.000" width="0.000" height="19.000" />
      <text class="left" x="363.780" y="174.000">systemd-ask-password-console.path</text>
      <rect class="activating" x="358.802" y="180.000" width="0.000" height="19.000" />
      <rect class="active" x="358.802" y="180.000" width="445.569" height="19.000" />
      <rect class="deactivating" x="804.370" y="180.000" width="0.000" height="19.000" />
      <text class="left" x="363.802" y="194.000">cryptsetup.target</text>
      <rect class="activating" x="358.957" y="200.000" width="0.000" height="19.000" />
      <rect class="active" x="358.957" y="200.000" width="445.413" height="19.000" />
      <rect class="deactivating" x="804.370" y="200.000" width="0.000" height="19.000" />
      <text class="left" x="363.957" y="214.000">systemd-udevd-kernel.socket</text>
      <rect class="activating" x="359.439" y="220.000" width="0.000" height="19.000" />
      <rect class="active" x="359.439" y="220.000" width="444.931" height="19.000" />
      <rect class="deactivating" x="804.370" y="220.000" width="0.000" height="19.000" />
      <text class="left" x="364.439" y="234.000">systemd-initctl.socket</text>
      <rect class="activating" x="359.670" y="240.000" width="0.000" height="19.000" />
      <rect class="active" x="359.670" y="240.000" width="444.700" height="19.000" />
      <rect class="deactivating" x="804.370" y="240.000" width="0.000" height="19.000" />
      <text class="left" x="364.670" y="254.000">user.slice</text>
      <rect class="activating" x="359.788" y="260.000" width="0.000" height="19.000" />
      <rect class="active" x="359.788" y="260.000" width="444.583" height="19.000" />
      <rect class="deactivating" x="804.370" y="260.000" width="0.000" height="19.000" />
      <text class="left" x="364.788" y="274.000">system-sshd\x2dkeygen.slice</text>
      <rect class="activating" x="360.139" y="280.000" width="0.000" height="19.000" />
      <rect class="active" x="360.139" y="280.000" width="444.232" height="19.000" />
      <rect class="deactivating" x="804.370" y="280.000" width="0.000" height="19.000" />
      <text class="left" x="365.139" y="294.000">systemd-coredump.socket</text>
      <rect class="activating" x="361.728" y="300.000" width="3.475" height="19.000" />
      <rect class="active" x="365.203" y="300.000" width="439.168" height="19.000" />
      <rect class="deactivating" x="804.370" y="300.000" width="0.000" height="19.000" />
      <text class="left" x="366.728" y="314.000">dev-mqueue.mount (34ms)</text>
      <rect class="activating" x="362.169" y="320.000" width="0.000" height="19.000" />
      <rect class="active" x="362.169" y="320.000" width="442.201" height="19.000" />
      <rect class="deactivating" x="804.370" y="320.000" width="0.000" height="19.000" />
      <text class="left" x="367.169" y="334.000">systemd-udevd-control.socket</text>
      <rect class="activating" x="362.960" y="340.000" width="36.463" height="19.000" />
      <rect class="active" x="399.423" y="340.000" width="404.947" height="19.000" />
      <rect class="deactivating" x="804.370" y="340.000" width="0.000" height="19.000" />
      <text class="left" x="367.960" y="354.000">systemd-udev-trigger.service (364ms)</text>
      <rect class="activating" x="364.567" y="360.000" width="0.000" height="19.000" />
      <rect class="active" x="364.567" y="360.000" width="439.803" height="19.000" />
      <rect class="deactivating" x="804.370" y="360.000" width="0.000" height="19.000" />
      <text class="left" x="369.567" y="374.000">proc-sys-fs-binfmt_misc.automount</text>
      <rect class="activating" x="364.695" y="380.000" width="0.000" height="19.000" />
      <rect class="active" x="364.695" y="380.000" width="439.676" height="19.000" />
      <rect class="deactivating" x="804.370" y="380.000" width="0.000" height="19.000" />
      <text class="left" x="369.695" y="394.000">slices.target</text>
      <rect class="activating" x="364.699" y="400.000" width="0.000" height="19.000" />
      <rect class="active" x="364.699" y="400.000" width="439.672" height="19.000" />
      <rect class="deactivating" x="804.370" y="400.000" width="0.000" height="19.000" />
      <text class="left" x="369.699" y="414.000">paths.target</text>
      <rect class="activating" x="364.705" y="420.000" width="0.000" height="19.000" />
      <rect class="active" x="364.705" y="420.000" width="439.665" height="19.000" />
      <rect class="deactivating" x="804.370" y="420.000" width="0.000" height="19.000" />
      <text class="left" x="369.705" y="434.000">rpcbind.target</text>
      <rect class="activating" x="364.728" y="440.000" width="0.000" height="19.000" />
      <rect class="active" x="364.728" y="440.000" width="439.642" height="19.000" />
      <rect class="deactivating" x="804.370" y="440.000" width="0.000" height="19.000" />
      <text class="left" x="369.728" y="454.000">system-getty.slice</text>
      <rect class="activating" x="364.801" y="460.000" width="14.014" height="19.000" />
      <rect class="active" x="378.816" y="460.000" width="425.555" height="19.000" />
      <rect class="deactivating" x="804.370" y="460.000" width="0.000" height="19.000" />
      <text class="left" x="369.801" y="474.000">systemd-sysctl.service (140ms)</text>
      <rect class="activating" x="364.912" y="480.000" width="7.556" height="19.000" />
      <rect class="active" x="372.468" y="480.000" width="431.903" height="19.000" />
      <rect class="deactivating" x="804.370" y="480.000" width="0.000" height="19.000" />
      <text class="left" x="369.912" y="494.000">systemd-remount-fs.service (75ms)</text>
      <rect class="activating" x="364.975" y="500.000" width="3.944" height="19.000" />
      <rect class="active" x="368.919" y="500.000" width="435.451" height="19.000" />
      <rect class="deactivating" x="804.370" y="500.000" width="0.000" height="19.000" />
      <text class="left" x="369.975" y="514.000">nis-domainname.service (39ms)</text>
      <rect class="activating" x="372.892" y="520.000" width="89.223" height="19.000" />
      <rect class="active" x="462.114" y="520.000" width="342.256" height="19.000" />
      <rect class="deactivating" x="804.370" y="520.000" width="0.000" height="19.000" />
      <text class="left" x="377.892" y="534.000">systemd-hwdb-update.service (892ms)</text>
      <rect class="activating" x="374.098" y="540.000" width="12.546" height="19.000" />
      <rect class="active" x="386.644" y="540.000" width="417.726" height="19.000" />
      <rect class="deactivating" x="804.370" y="540.000" width="0.000" height="19.000" />
      <text class="left" x="379.098" y="554.000">systemd-sysusers.service (125ms)</text>
      <rect class="activating" x="376.144" y="560.000" width="8.475" height="19.000" />
      <rect class="active" x="384.619" y="560.000" width="419.752" height="19.000" />
      <rect class="deactivating" x="804.370" y="560.000" width="0.000" height="19.000" />
      <text class="left" x="381.144" y="574.000">systemd-journal-flush.service (84ms)</text>
      <rect class="activating" x="376.937" y="580.000" width="7.207" height="19.000" />
      <rect class="active" x="384.144" y="580.000" width="420.226" height="19.000" />
      <rect class="deactivating" x="804.370" y="580.000" width="0.000" height="19.000" />
      <text class="left" x="381.937" y="594.000">systemd-random-seed.service (72ms)</text>
      <rect class="activating" x="377.918" y="600.000" width="8.460" height="19.000" />
      <rect class="active" x="386.378" y="600.000" width="417.993" height="19.000" />
      <rect class="deactivating" x="804.370" y="600.000" width="0.000" height="19.000" />
      <text class="left" x="382.918" y="614.000">swapfile.swap (84ms)</text>
      <rect class="activating" x="386.397" y="620.000" width="0.000" height="19.000" />
      <rect class="active" x="386.397" y="620.000" width="417.974" height="19.000" />
      <rect class="deactivating" x="804.370" y="620.000" width="0.000" height="19.000" />
      <text class="left" x="391.397" y="634.000">swap.target</text>
      <rect class="activating" x="386.726" y="640.000" width="8.913" height="19.000" />
      <rect class="active" x="395.639" y="640.000" width="408.731" height="19.000" />
      <rect class="deactivating" x="804.370" y="640.000" width="0.000" height="19.000" />
      <text class="left" x="391.726" y="654.000">systemd-tmpfiles-setup-dev.service (89ms)</text>
      <rect class="activating" x="395.659" y="660.000" width="0.000" height="19.000" />
      <rect class="active" x="395.659" y="660.000" width="408.712" height="19.000" />
      <rect class="deactivating" x="804.370" y="660.000" width="0.000" height="19.000" />
      <text class="left" x="400.659" y="674.000">local-fs-pre.target</text>
      <rect class="activating" x="395.662" y="680.000" width="0.000" height="19.000" />
      <rect class="active" x="395.662" y="680.000" width="408.708" height="19.000" />
      <rect class="deactivating" x="804.370" y="680.000" width="0.000" height="19.000" />
      <text class="left" x="400.662" y="694.000">local-fs.target</text>
      <rect class="activating" x="395.765" y="700.000" width="4.943" height="19.000" />
      <rect class="active" x="400.708" y="700.000" width="403.663" height="19.000" />
      <rect class="deactivating" x="804.370" y="700.000" width="0.000" height="19.000" />
      <text class="left" x="400.765" y="714.000">dracut-shutdown.service (49ms)</text>
      <rect class="activating" x="395.900" y="720.000" width="15.206" height="19.000" />
      <rect class="active" x="411.107" y="720.000" width="393.264" height="19.000" />
      <rect class="deactivating" x="804.370" y="720.000" width="0.000" height="19.000" />
      <text class="left" x="400.900" y="734.000">import-state.service (152ms)</text>
      <rect class="activating" x="396.029" y="740.000" width="8.903" height="19.000" />
      <rect class="active" x="404.932" y="740.000" width="399.438" height="19.000" />
      <rect class="deactivating" x="804.370" y="740.000" width="0.000" height="19.000" />
      <text class="left" x="401.029" y="754.000">systemd-journal-catalog-update.service (89ms)</text>
      <rect class="activating" x="396.120" y="760.000" width="37.072" height="19.000" />
      <rect class="active" x="433.192" y="760.000" width="371.179" height="19.000" />
      <rect class="deactivating" x="804.370" y="760.000" width="0.000" height="19.000" />
      <text class="left" x="401.120" y="774.000">ldconfig.service (370ms)</text>
      <rect class="activating" x="396.205" y="780.000" width="8.788" height="19.000" />
      <rect class="active" x="404.993" y="780.000" width="399.378" height="19.000" />
      <rect class="deactivating" x="804.370" y="780.000" width="0.000" height="19.000" />
      <text class="left" x="401.205" y="794.000">systemd-machine-id-commit.service (87ms)</text>
      <rect class="activating" x="411.239" y="800.000" width="14.877" height="19.000" />
      <rect class="active" x="426.115" y="800.000" width="378.255" height="19.000" />
      <rect class="deactivating" x="804.370" y="800.000" width="0.000" height="19.000" />
      <text class="right" x="406.239" y="814.000">systemd-tmpfiles-setup.service (148ms)</text>
      <rect class="activating" x="426.321" y="820.000" width="18.570" height="19.000" />
      <rect class="active" x="444.892" y="820.000" width="359.478" height="19.000" />
      <rect class="deactivating" x="804.370" y="820.000" width="0.000" height="19.000" />
      <text class="right" x="421.321" y="834.000">var-lib-nfs-rpc_pipefs.mount (185ms)</text>
      <rect class="activating" x="444.905" y="840.000" width="0.000" height="19.000" />
      <rect class="active" x="444.905" y="840.000" width="359.465" height="19.000" />
      <rect class="deactivating" x="804.370" y="840.000" width="0.000" height="19.000" />
      <text class="right" x="439.905" y="854.000">rpc_pipefs.target</text>
      <rect class="activating" x="450.095" y="860.000" width="1.722" height="19.000" />
      <rect class="active" x="451.817" y="860.000" width="352.554" height="19.000" />
      <rect class="deactivating" x="804.370" y="860.000" width="0.000" height="19.000" />
      <text class="right" x="445.095" y="874.000">systemd-update-utmp.service (17ms)</text>
      <rect class="activating" x="462.330" y="880.000" width="2.908" height="19.000" />
      <rect class="active" x="465.238" y="880.000" width="339.132" height="19.000" />
      <rect class="deactivating" x="804.370" y="880.000" width="0.000" height="19.000" />
      <text class="right" x="457.330" y="894.000">systemd-update-done.service (29ms)</text>
      <rect class="activating" x="469.136" y="900.000" width="0.000" height="19.000" />
      <rect class="active" x="469.136" y="900.000" width="335.234" height="19.000" />
      <rect class="deactivating" x="804.370" y="900.000" width="0.000" height="19.000" />
      <text class="right" x="464.136" y="914.000">sysinit.target</text>
      <rect class="activating" x="469.342" y="920.000" width="0.000" height="19.000" />
      <rect class="active" x="469.342" y="920.000" width="335.028" height="19.000" />
      <rect class="deactivating" x="804.370" y="920.000" width="0.000" height="19.000" />
      <text class="right" x="464.342" y="934.000">dbus.socket</text>
      <rect class="activating" x="469.361" y="940.000" width="0.000" height="19.000" />
      <rect class="active" x="469.361" y="940.000" width="335.010" height="19.000" />
      <rect class="deactivating" x="804.370" y="940.000" width="0.000" height="19.000" />
      <text class="right" x="464.361" y="954.000">sockets.target</text>
      <rect class="activating" x="469.372" y="960.000" width="0.000" height="19.000" />
      <rect class="active" x="469.372" y="960.000" width="334.998" height="19.000" />
      <rect class="deactivating" x="804.370" y="960.000" width="0.000" height="19.000" />
      <text class="right" x="464.372" y="974.000">systemd-tmpfiles-clean.timer</text>
      <rect class="activating" x="469.398" y="980.000" width="0.000" height="19.000" />
      <rect class="active" x="469.398" y="980.000" width="334.972" height="19.000" />
      <rect class="deactivating" x="804.370" y="980.000" width="0.000" height="19.000" />
      <text class="right" x="464.398" y="994.000">basic.target</text>
      <rect class="activating" x="471.160" y="1000.000" width="35.834" height="19.000" />
      <rect class="active" x="506.994" y="1000.000" width="0.000" height="19.000" />
      <rect class="deactivating" x="506.994" y="1000.000" width="0.000" height="19.000" />
      <text class="right" x="466.160" y="1014.000">sshd-keygen@ecdsa.service (358ms)</text>
      <rect class="activating" x="474.251" y="1020.000" width="0.000" height="19.000" />
      <rect class="active" x="474.251" y="1020.000" width="17.023" height="19.000" />
      <rect class="deactivating" x="491.274" y="1020.000" width="0.000" height="19.000" />
      <text class="right" x="469.251" y="1034.000">irqbalance.service</text>
      <rect class="activating" x="475.603" y="1040.000" width="134.160" height="19.000" />
      <rect class="active" x="609.763" y="1040.000" width="0.000" height="19.000" />
      <rect class="deactivating" x="609.763" y="1040.000" width="0.000" height="19.000" />
      <text class="right" x="470.603" y="1054.000">sshd-keygen@rsa.service (1.341s)</text>
      <rect class="activating" x="476.995" y="1060.000" width="0.000" height="19.000" />
      <rect class="active" x="476.995" y="1060.000" width="327.375" height="19.000" />
      <rect class="deactivating" x="804.370" y="1060.000" width="0.000" height="19.000" />
      <text class="right" x="471.995" y="1074.000">dbus.service</text>
      <rect class="activating" x="486.399" y="1080.000" width="38.754" height="19.000" />
      <rect class="active" x="525.152" y="1080.000" width="0.000" height="19.000" />
      <rect class="deactivating" x="525.152" y="1080.000" width="0.000" height="19.000" />
      <text class="right" x="481.399" y="1094.000">sshd-keygen@ed25519.service (387ms)</text>
      <rect class="activating" x="488.604" y="1100.000" width="0.000" height="19.000" />
      <rect class="active" x="488.604" y="1100.000" width="315.766" height="19.000" />
      <rect class="deactivating" x="804.370" y="1100.000" width="0.000" height="19.000" />
      <text class="right" x="483.604" y="1114.000">timers.target</text>
      <rect class="activating" x="495.400" y="1120.000" width="0.000" height="19.000" />
      <rect class="active" x="495.400" y="1120.000" width="308.971" height="19.000" />
      <rect class="deactivating" x="804.370" y="1120.000" width="0.000" height="19.000" />
      <text class="right" x="490.400" y="1134.000">remote-fs-pre.target</text>
      <rect class="activating" x="495.418" y="1140.000" width="0.000" height="19.000" />
      <rect class="active" x="495.418" y="1140.000" width="308.953" height="19.000" />
      <rect class="deactivating" x="804.370" y="1140.000" width="0.000" height="19.000" />
      <text class="right" x="490.418" y="1154.000">remote-fs.target</text>
      <rect class="activating" x="538.800" y="1160.000" width="0.000" height="19.000" />
      <rect class="active" x="538.800" y="1160.000" width="265.570" height="19.000" />
      <rect class="deactivating" x="804.370" y="1160.000" width="0.000" height="19.000" />
      <text class="right" x="533.800" y="1174.000">network.target</text>
      <rect class="activating" x="665.773" y="1180.000" width="0.000" height="19.000" />
      <rect class="active" x="665.773" y="1180.000" width="138.598" height="19.000" />
      <rect class="deactivating" x="804.370" y="1180.000" width="0.000" height="19.000" />
      <text class="right" x="660.773" y="1194.000">network-online.target</text>
      <rect class="activating" x="666.243" y="1200.000" width="21.576" height="19.000" />
      <rect class="active" x="687.819" y="1200.000" width="0.000" height="19.000" />
      <rect class="deactivating" x="687.819" y="1200.000" width="0.000" height="19.000" />
      <text class="right" x="661.243" y="1214.000">kdump.service (215ms)</text>
      <rect class="activating" x="666.329" y="1220.000" width="7.861" height="19.000" />
      <rect class="active" x="674.190" y="1220.000" width="130.180" height="19.000" />
      <rect class="deactivating" x="804.370" y="1220.000" width="0.000" height="19.000" />
      <text class="right" x="661.329" y="1234.000">rpc-statd-notify.service (78ms)</text>
      <rect class="activating" x="669.212" y="1240.000" width="0.000" height="19.000" />
      <rect class="active" x="669.212" y="1240.000" width="135.159" height="19.000" />
      <rect class="deactivating" x="804.370" y="1240.000" width="0.000" height="19.000" />
      <text class="right" x="664.212" y="1254.000">nss-user-lookup.target</text>
      <rect class="activating" x="669.403" y="1260.000" width="10.489" height="19.000" />
      <rect class="active" x="679.892" y="1260.000" width="124.478" height="19.000" />
      <rect class="deactivating" x="804.370" y="1260.000" width="0.000" height="19.000" />
      <text class="right" x="664.403" y="1274.000">systemd-user-sessions.service (104ms)</text>
      <rect class="activating" x="674.001" y="1280.000" width="19.849" height="19.000" />
      <rect class="active" x="693.851" y="1280.000" width="110.520" height="19.000" />
      <rect class="deactivating" x="804.370" y="1280.000" width="0.000" height="19.000" />
      <text class="right" x="669.001" y="1294.000">systemd-logind.service (198ms)</text>
      <rect class="activating" x="680.217" y="1300.000" width="0.000" height="19.000" />
      <rect class="active" x="680.217" y="1300.000" width="124.153" height="19.000" />
      <rect class="deactivating" x="804.370" y="1300.000" width="0.000" height="19.000" />
      <text class="right" x="675.217" y="1314.000">getty@tty1.service</text>
      <rect class="activating" x="680.269" y="1320.000" width="0.000" height="19.000" />
      <rect class="active" x="680.269" y="1320.000" width="124.101" height="19.000" />
      <rect class="deactivating" x="804.370" y="1320.000" width="0.000" height="19.000" />
      <text class="right" x="675.269" y="1334.000">getty.target</text>
      <rect class="activating" x="736.122" y="1340.000" width="0.000" height="19.000" />
      <rect class="active" x="736.122" y="1340.000" width="68.248" height="19.000" />
      <rect class="deactivating" x="804.370" y="1340.000" width="0.000" height="19.000" />
      <text class="right" x="731.122" y="1354.000">sys-devices-pci0000:00-0000:00:05.0-sound-card0.device</text>
      <rect class="activating" x="736.277" y="1360.000" width="0.000" height="19.000" />
      <rect class="active" x="736.277" y="1360.000" width="68.094" height="19.000" />
      <rect class="deactivating" x="804.370" y="1360.000" width="0.000" height="19.000" />
      <text class="right" x="731.277" y="1374.000">sound.target</text>
      <rect class="activating" x="799.477" y="1380.000" width="0.000" height="19.000" />
      <rect class="active" x="799.477" y="1380.000" width="4.893" height="19.000" />
      <rect class="deactivating" x="804.370" y="1380.000" width="0.000" height="19.000" />
      <text class="right" x="794.477" y="1394.000">multi-user.target</text>
      <rect class="activating" x="799.611" y="1400.000" width="4.721" height="19.000" />
      <rect class="active" x="804.331" y="1400.000" width="0.000" height="19.000" />
      <rect class="deactivating" x="804.331" y="1400.000" width="0.000" height="19.000" />
      <text class="right" x="794.611" y="1414.000">systemd-update-utmp-runlevel.service (47ms)</text>
    </g>
    <g transform="translate(20,100)">
      <rect class="activating" x="0.000" y="1440.000" width="30.000" height="19.000" />
      <text class="left" x="45.000" y="1454.000">Activating</text>
      <rect class="active" x="0.000" y="1460.000" width="30.000" height="19.000" />
      <text class="left" x="45.000" y="1474.000">Active</text>
      <rect class="deactivating" x="0.000" y="1480.000" width="30.000" height="19.000" />
      <text class="left" x="45.000" y="1494.000">Deactivating</text>
      <rect class="security" x="0.000" y="1500.000" width="30.000" height="19.000" />
      <text class="left" x="45.000" y="1514.000">Setting up security module</text>
      <rect class="generators" x="0.000" y="1520.000" width="30.000" height="19.000" />
      <text class="left" x="45.000" y="1534.000">Generators</text>
      <rect class="unitsload" x="0.000" y="1540.000" width="30.000" height="19.000" />
      <text class="left" x="45.000" y="1554.000">Loading unit files</text>
    </g>    
    </svg>



